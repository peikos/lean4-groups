-- This module serves as the root of the `Groups` library.
-- Import modules here that should be built as part of the library.
import «Groups».Basic
import Mathlib.Data.Set.Basic
import Mathlib.Tactic.NthRewrite

class magma (α : Type u) where
  mappend : α → α → α

infixl:80 "*" => magma.mappend

namespace magma

variable {α : Type u}  [magma α]

axiom left_mappend (n : α) {a b : α} : a = b → n * a = n * b

axiom right_mappend (n : α) {a b : α} : a = b → a * n = b * n -- TODO proof one from the other

end magma



class has_one (α : Type u) where
  one : α

notation:90 "𝟙" => has_one.one


class semigroup (α : Type u) extends magma α where
  mappend_assoc : ∀ a b c : α, (a * b) * c = a * (b * c)


class monoid (α : Type u) extends semigroup α, has_one α where
  left_unit : ∀ a : α, one * a = a
  right_unit : ∀ a : α, a * one = a

namespace monoid

variable {α : Type u} [monoid α]

theorem id_unique (e₁ e₂ : α) (h : ∀ a : α, e₁ * a = a ∧ a * e₂ = a) : e₁ = e₂ := by
  have ⟨ h₁, h₂ ⟩  := h has_one.one

  rw [ monoid.left_unit e₂
     ] at h₂

  rw [ monoid.right_unit e₁
     , ← h₂
     ] at h₁

  exact h₁

end monoid


class has_inv (α : Type u) where
  inv : α → α

postfix:99 "⁻¹" => has_inv.inv


class group (α : Type u) extends monoid α, has_inv α where
  left_inv : ∀ a : α, a⁻¹ * a = one
  right_inv : ∀ a : α, a * a⁻¹ = one

namespace group

variable {α : Type u}  [group α]

theorem inv_unique (a f₁ f₂ : α) (h₁ : f₁ * a = 𝟙) (h₂ : a * f₂ = 𝟙) : f₁ = f₂ := by
rw [ ← monoid.right_unit f₁
   , ← h₂
   , ← semigroup.mappend_assoc f₁ a f₂
   , h₁
   , monoid.left_unit f₂
   ]

end group


namespace group

variable {α : Type u}  [group α]

theorem mappend_left_cancel (n : α) {a b : α} : n * a = n * b ↔ a = b := by
constructor

intro h
rw [ ← monoid.left_unit a
   , ← monoid.left_unit b
   , ← group.left_inv n
   , semigroup.mappend_assoc
   , semigroup.mappend_assoc
   , magma.left_mappend
   ]
exact h

intro h
rw [magma.left_mappend]
exact h

theorem mappend_right_cancel (n : α) {a b : α} : a * n = b * n ↔ a = b := by
constructor

intro h
rw [ ← monoid.right_unit a
   , ← monoid.right_unit b
   , ← group.right_inv n
   , ← semigroup.mappend_assoc
   , ← semigroup.mappend_assoc
   , magma.right_mappend
   ]
exact h

intro h
rw [magma.right_mappend]
exact h

end group


namespace group

variable {α : Type u}  [group α]

theorem inv_inv (a : α) : (a⁻¹)⁻¹ = a := by
rw [ ← mappend_left_cancel (a⁻¹)
   , group.left_inv
   , group.right_inv
   ]

end group







namespace group

open Set

variable {α : Type u} {H : Type v} --[group G] [group H]

--axiom subgroup {α : Type u} : [g h : group α]  (∃ iso : h → g, ∀ eg : g, ∃ eh : h, g = iso h)
class subgroup [group α] (S : Set α) : Prop :=
  (mappend_mem : ∀ {a b}, a ∈ S → b ∈ S → magma.mappend a b ∈ S)
  (one_mem : (𝟙 : α) ∈ S)
  (inv_mem : ∀ {a}, a ∈ S → a⁻¹ ∈ S)

end group

namespace group

def trivial_group (G : Type u) [group G] : Set G := {𝟙}

instance trivial_subgroup [group G] : subgroup (trivial_group G) := by
constructor

-- Closure
intro x y x_in_g y_in_g

rw [ trivial_group
   , Set.mem_singleton_iff
   ] at x_in_g

rw [ trivial_group
   , Set.mem_singleton_iff
   ] at y_in_g

rw [ x_in_g
   , y_in_g
   , trivial_group
   , Set.mem_singleton_iff
   , monoid.left_unit
   ]

-- Unit
rw [ trivial_group, Set.mem_singleton_iff]

-- Inverse
intro x x_in_g

rw [ trivial_group
   , Set.mem_singleton_iff
   ] at x_in_g

rw [ trivial_group
   , Set.mem_singleton_iff
   , ← group.left_inv x
   ]

nth_rewrite 3 [x_in_g]
rw [monoid.right_unit]







namespace group

open Set

class morphism ( α β : Type u ) [group α] [group β]  where
  mapping : α → β
  preserve : ∀ a b : α, mapping (a * b) = mapping a * mapping b

end group




inductive Trivial : Type u where
  | One : Trivial
  deriving Repr

instance : has_one Trivial where
  one := Trivial.One

instance : magma Trivial where
  mappend := λ _ _ ↦ 𝟙

instance : semigroup Trivial where
  mappend_assoc := by
    intros a b c
    cases a with
    | One => cases b with
      | One => cases c with
        | One => rw [magma.mappend]

instance : monoid Trivial where
  left_unit := by
    intro a
    cases a with
    | One => rw [magma.mappend]
  right_unit := by
    intro a
    cases a with
    | One => rw [magma.mappend]

instance : has_inv Trivial where
  inv := λ _ ↦ 𝟙

instance : group Trivial where
  left_inv := by
    intros a
    cases a with
    | One => rw [has_inv.inv, magma.mappend]
  right_inv := by
    intros a
    cases a with
    | One => rw [has_inv.inv, magma.mappend]




namespace group

variable {G : Type u}  [group G]

instance zero_morphism : morphism G Trivial where
  mapping := λ _ ↦ 𝟙
  preserve := by simp

end group






inductive C2 where
  | One | A
  deriving Repr

namespace C2

-- Extensionality theorem for C2
theorem ext (x y : C2) (h : ∀ z : C2, z = x ↔ z = y) : x = y := by
  cases x with
  | One =>
    cases y with
    | One =>
      constructor
    | A =>
      rw [← h]
  | A =>
    cases y with
    | A =>
      constructor
    | One =>
      rw [← h]

end C2

@[simp]
--def mappend_c2 : C2 → C2 → C2
def mappend_c2 (x y : C2) : C2 :=
  match x,y with
  | C2.One, x => x
  | x, C2.One => x
  | C2.A, C2.A => C2.One

instance : magma C2 where
  mappend := mappend_c2

instance : has_one C2 where
  one := C2.One

open C2

theorem c2_assoc : ∀ a b c : C2, (a * b) * c = a * (b * c) := by
intros a b c
cases a with
  | One =>
    cases b with
    | One =>
      cases c with
      | One =>
        rfl
      | A => rfl
      --apply mappend_c2 One c
    | A =>
      cases c with
      | One => rfl
      | A => rfl
  | A =>
    cases b with
    | One =>
      cases c with
      | One => rfl
      | A => rfl
    | A =>
      cases c with
      | One => rfl
      | A => rfl

instance : semigroup C2 where
  mappend_assoc := c2_assoc

theorem c2_left_id : ∀ a : C2, C2.One * a = a := by
intros a
cases a with
  | One => rfl
  | A => rfl
  
theorem c2_right_id : ∀ a : C2, a * C2.One = a := by
intros a
cases a with
  | One => rfl
  | A => rfl
  
instance : monoid C2 where
  left_unit := c2_left_id
  right_unit := c2_right_id

instance : has_inv C2 where
  inv := mappend_c2 C2.One

theorem c2_left_inv : ∀ a : C2, a⁻¹ * a = 𝟙 := by
intros a
cases a with
  | One => rfl
  | A => rfl

theorem c2_right_inv : ∀ a : C2, a * a⁻¹ = 𝟙 := by
intros a
cases a with
  | One => rfl
  | A => rfl
  
instance : group C2 where
  left_inv := c2_left_inv
  right_inv := c2_right_inv
  

def triv_c2 : Set C2 := {𝟙}



open group
#check morphism C2 Trivial
#eval C2.A
#eval zero_morphism.mapping (C2.A) * zero_morphism.mapping (C2.A)
#eval zero_morphism.mapping (C2.A * C2.A)
end group



def lex (x y : Ordering) : Ordering :=
  match x with
  | Ordering.eq => y
  | _ => x

instance : magma Ordering  where
  mappend := lex

instance : magma Nat where
  mappend := Nat.mul
